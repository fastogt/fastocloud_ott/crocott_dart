import 'package:crocott_dart/crocott_public.dart';
import 'package:test/test.dart';

void main() {
  test('crocottline', () async {
    final result = getCrocOTTLineBrands(Uri.parse('https://line.crocott.com/brands'));
    result.then((brands) {
      //expect(brands, hasLength(1));
      expect(brands.isNotEmpty, true);
    });
    expect(result, completes);

    final ress = PackageStats.fromJson({
      'live': {'123': 1},
      'online_clients': 15
    });
    expect(ress.onlineClients, 15);
    expect(ress.live.length, 1);
  });
}
