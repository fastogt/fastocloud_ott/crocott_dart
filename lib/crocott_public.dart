import 'dart:io';

import 'package:crocott_dart/errors.dart';
import 'package:crocott_dart/types.dart';
import 'package:dart_common/dart_common.dart';
import 'package:fastotv_dart/commands_info/ott_server_info.dart';
import 'package:fastotv_dart/commands_info/public_package.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:fastotv_dart/profile.dart';
import 'package:http/http.dart' as http;

class LocalesResp {
  final List<List<String>> countries;
  final List<List<String>> languages;
  final String currentCountry;
  final String currentLanguage;
  final LatLng location;

  LocalesResp(
      this.countries, this.languages, this.currentCountry, this.currentLanguage, this.location);
}

class PackageStats {
  static const _ONLINE_CLIENTS_FIELD = 'online_clients';
  static const _LIVE_FIELD = 'live';
  static const _STATS_FIELD = 'stats';

  final int onlineClients;
  final Map<String, int> live;
  final PackageDBStats? stats;

  PackageStats({required this.onlineClients, required this.live, this.stats});

  PackageStats copy() {
    return PackageStats(onlineClients: onlineClients, live: live, stats: stats);
  }

  factory PackageStats.fromJson(Map<String, dynamic> json) {
    final oc = json[_ONLINE_CLIENTS_FIELD];
    final lv = json[_LIVE_FIELD];
    final live = <String, int>{};
    Map.castFrom(lv).forEach((key, val) {
      live[key] = val;
    });

    PackageDBStats? stats;
    if (json.containsKey(_STATS_FIELD)) {
      stats = PackageDBStats.fromJson(json[_STATS_FIELD]);
    }
    return PackageStats(onlineClients: oc, live: live, stats: stats);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {_ONLINE_CLIENTS_FIELD: onlineClients, _LIVE_FIELD: live};
    if (stats != null) {
      result[_STATS_FIELD] = stats!.toJson();
    }
    return result;
  }
}

class OttServerInfoEx extends OttServerInfo {
  static const _HOST_FIELD = 'host';

  String host;

  OttServerInfoEx(
      {required this.host,
      required int providersCount,
      required OttSettings brand,
      required List<PackagePublic> packages})
      : super(providersCount: providersCount, brand: brand, packages: packages);

  OttServerInfoEx.info(
      {required this.host, required OttServerInfo info, required List<PackagePublic> packages})
      : super(providersCount: info.providersCount, brand: info.brand, packages: packages);

  @override
  OttServerInfoEx copy() {
    return OttServerInfoEx(
        host: host, providersCount: providersCount, brand: brand.copy(), packages: packages);
  }

  factory OttServerInfoEx.fromJson(Map<String, dynamic> json) {
    final base = OttServerInfo.fromJson(json);
    final host = json[_HOST_FIELD];
    return OttServerInfoEx(
        host: host,
        providersCount: base.providersCount,
        brand: base.brand,
        packages: base.packages);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[_HOST_FIELD] = host;
    return result;
  }
}

Future<List<OttServerInfoEx>> getCrocOTTLineBrands(Uri url) {
  final resp = http.get(url); // OK
  return resp.then((value) {
    final respData = httpDataResponseFromString(value.body);
    if (respData == null) {
      throw ErrorHttp(value.statusCode, value.reasonPhrase);
    }

    if (value.statusCode == HttpStatus.ok) {
      final data = respData.contentMap()!;
      final List<OttServerInfoEx> result = [];
      data['brands'].forEach((d) {
        final res = OttServerInfoEx.fromJson(d);
        result.add(res);
      });
      return result;
    }

    throw ErrorHttp(value.statusCode, value.reasonPhrase, respData.error());
  }, onError: (error) {
    handleHttpError(error);
  });
}

class CrocOTTPublic extends IDartBearerFetcher {
  static const _API_VERSION = '/panel_pro/api';

  String host;

  CrocOTTPublic({required this.host});

  Future<LocalesResp> getLocale() {
    LatLng formatLocation(String coordinates) {
      final list = coordinates.split(',');
      final latitude = double.parse(list[0]);
      final longitude = double.parse(list[1]);
      return LatLng(latitude: latitude, longitude: longitude);
    }

    final resp = fetchGet('/locale'); // OK
    return resp.then((value) {
      final respData = httpDataResponseFromString(value.body);
      if (respData == null) {
        throw ErrorHttp(value.statusCode, value.reasonPhrase);
      }

      final data = respData.contentMap()!;
      final List<List<String>> _countries = [];
      final List<List<String>> _languages = [];
      for (final List c in data['countries']) {
        _countries.add(c.cast<String>());
      }
      for (final List l in data['languages']) {
        _languages.add(l.cast<String>());
      }
      final location = formatLocation(data['loc']);
      return LocalesResp(
          _countries, _languages, data['current_country'], data['current_language'], location);
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<OttServerInfo> getServerInfo() {
    final resp = fetchGet('/info'); // OK

    return resp.then((value) {
      final respData = httpDataResponseFromString(value.body);
      if (respData == null) {
        throw ErrorHttp(value.statusCode, value.reasonPhrase);
      }

      final data = respData.contentMap()!;
      return OttServerInfo.fromJson(data);
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<PackageStats> getPackageStats(String pid) {
    final resp = fetchGet('/package/$pid/stats');

    return resp.then((value) {
      final respData = httpDataResponseFromString(value.body);
      if (respData == null) {
        throw ErrorHttp(value.statusCode, value.reasonPhrase);
      }

      final data = respData.contentMap()!;
      return PackageStats.fromJson(data);
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<SubProfile> signupClient(SubscriberSignUpFront profile) {
    final resp = fetchPost('/client/signup', profile.toJson());
    return resp.then((value) {
      final respData = httpDataResponseFromString(value.body);
      if (respData == null) {
        throw ErrorHttp(value.statusCode, value.reasonPhrase);
      }

      final data = respData.contentMap()!;
      return SubProfile.fromJson(data);
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<SubProfile> signupSubscriber(SubscriberSignUpFront profile) {
    final resp = fetchPost('/subscriberui/signup', profile.toJson(), [200, 201]);
    return resp.then((value) {
      final respData = httpDataResponseFromString(value.body);
      if (respData == null) {
        throw ErrorHttp(value.statusCode, value.reasonPhrase);
      }

      final data = respData.contentMap()!;
      return SubProfile.fromJson(data);
    }, onError: (error) {
      handleError(error);
    });
  }

  // protected:
  void handleError(dynamic error) {
    handleHttpError(error);
  }

  @override
  Uri getBackendEndpoint(String path) {
    final String base = '$host$_API_VERSION';
    return Uri.parse('$base$path');
  }
}
