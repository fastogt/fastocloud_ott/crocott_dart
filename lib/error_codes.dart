final class ErrorCodes {
  static const int kErrInvalidInput = -5000;
  static const int kErrInternal = -5001; //
  static const int kErrParseResponse = -5002; //
  static const int kErrForrbiddenAction = -5003;
  static const int kErrAuth = -5004;
  static const int kErrBadRequestData = -5005;
  static const int kErrNotFound = -5006;
  static const int kErrGenerateToken = -5007;
  static const int kErrQueryParams = -5008;
  static const int kErrForrbiddenByConflict = -5009; //

  // clientErrors getDevices, requestDevice and logins
  static const int kClientErrLoginOrPass = -5020;
  static const int kClientErrCode = -5021;
  static const int kClientErrBannedDevice = -5022;
  static const int kClientErrExpiredAccount = -5023;
  static const int kClientErrMaxCountDevices = -5024;
  static const int kClientErrNotActive = -5025;
  static const int kClientErrNotFoundDev = -5026;

  static const int kClientErrAlreadySignup = -5030;
}
