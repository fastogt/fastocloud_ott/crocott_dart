import 'dart:io';

import 'package:dart_common/errors.dart';
import 'package:http/http.dart' as http;

class NeedHelpError extends IError {
  final String text;

  NeedHelpError(this.text);

  @override
  String error() {
    return text;
  }
}

class SocketError extends IError {
  final SocketException exc;

  SocketError(this.exc);

  @override
  String error() {
    return exc.osError?.message ?? exc.message;
  }
}

class ClientException extends IError {
  final http.ClientException exc;

  ClientException(this.exc);

  @override
  String error() {
    return exc.message;
  }
}

class HandshakeError extends IError {
  final HandshakeException exc;

  HandshakeError(this.exc);

  @override
  String error() {
    return exc.osError?.message ?? exc.message;
  }
}

void handleHttpError(dynamic error) {
  if (error is SocketException) {
    throw SocketError(error);
  } else if (error is HandshakeException) {
    throw HandshakeError(error);
  } else if (error is http.ClientException) {
    throw ClientException(error);
  } else {
    throw error;
  }
}
