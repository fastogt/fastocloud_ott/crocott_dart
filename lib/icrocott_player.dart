import 'dart:convert';
import 'dart:io';

import 'package:crocott_dart/error_codes.dart';
import 'package:crocott_dart/errors.dart';
import 'package:crocott_dart/types.dart';
import 'package:dart_common/dart_common.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:fastotv_dart/profile.dart';
import 'package:http/http.dart' as http;

class Tokens {
  final String refresh;
  final String access;

  Tokens({required this.refresh, required this.access});
}

typedef OnTokenChangedCallback = void Function(Tokens);
typedef OnOriginalChangedCallback = void Function(String?);
typedef OnNeedHelpCallback = void Function();

class LivePageResp {
  LivePageResp(this.streams, this.cvlo);

  final List<ChannelInfo> streams;
  final CountAndVersionLO cvlo;
}

class VodsPageResp {
  VodsPageResp(this.vods, this.cvlo);

  final List<VodInfo> vods;
  final CountAndVersionLO cvlo;
}

class SerialsPageResp {
  SerialsPageResp(this.serials, this.cvlo);

  final List<SerialInfo> serials;
  final CountAndVersionLO cvlo;
}

class BasicAuthCrocOTTLogin extends IDartBasicAuthFetcher {
  static const _API_VERSION = '/panel_pro/api';

  String host;

  BasicAuthCrocOTTLogin({required this.host});

  @override
  Uri getBackendEndpoint(String path) {
    final String base = '$host$_API_VERSION';
    return Uri.parse('$base$path');
  }
}

class CodeAuthCrocOTTLogin extends IDartCodeAuthFetcher {
  static const _API_VERSION = '/panel_pro/api';

  String host;

  CodeAuthCrocOTTLogin({required this.host});

  @override
  Uri getBackendEndpoint(String path) {
    final String base = '$host$_API_VERSION';
    return Uri.parse('$base$path');
  }
}

class BasicCrocOTTPlayer {
  String host;
  IDartFetcher _impl;

  BasicCrocOTTPlayer.code({required this.host, required String code})
      : _impl = CodeAuthCrocOTTLogin(host: host) {
    (_impl as CodeAuthCrocOTTLogin).setCode(code);
  }
  BasicCrocOTTPlayer.auth({required this.host, required String login, required String password})
      : _impl = BasicAuthCrocOTTLogin(host: host) {
    (_impl as BasicAuthCrocOTTLogin).setAuth(login, password);
  }

  Future<List<DeviceInfo>> getDevices() {
    final resp = _impl.fetchGet('/client/devices');
    return resp.then((response) {
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<DeviceInfo> result = [];
        data['devices'].forEach((d) {
          final res = DeviceInfo.fromJson(d);
          result.add(res);
        });
        return result;
      }
      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleHttpError(error);
      return;
    });
  }

  Future<DeviceInfo> requestDevice(String name) {
    final resp = _impl.fetchPost('/client/devices/add', {'name': name});
    return resp.then((response) {
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return DeviceInfo.fromJson(data['device']);
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleHttpError(error);
      return;
    });
  }

  Future<Tokens?> login(ClientInfo client) {
    final resp = _impl.fetchPost('/client/login', client.toJson());
    return resp.then((response) {
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return Tokens(refresh: data['refresh_token'], access: data['access_token']);
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleHttpError(error);
      return;
    });
  }
}

abstract class ICrocOTTPlayer {
  static const _API_VERSION = '/panel_pro/api';

  String get directHost {
    return _host;
  }

  String _host;

  Tokens? _tokens;
  OnTokenChangedCallback? onTokenChanged;
  OnNeedHelpCallback? onNeedHelp;

  ICrocOTTPlayer({required String host, this.onTokenChanged, this.onNeedHelp}) : _host = host;

  void setTokens(Tokens tokens) {
    _tokens = tokens;
  }

  void setHost(String host) {
    _host = host;
  }

  String? getAccessToken() {
    if (_tokens != null) {
      return _tokens!.access;
    }
    return null;
  }

  void _setTokens(Tokens tokens) {
    setTokens(tokens);
    if (onTokenChanged != null) {
      onTokenChanged!.call(tokens);
    }
  }

  // basic auth
  Future<List<DeviceInfo>> getDevices(String login, String password) {
    final impl = BasicCrocOTTPlayer.auth(host: _host, login: login, password: password);
    return impl.getDevices();
  }

  // basic auth
  Future<DeviceInfo> requestDevice(String login, String password, String name) {
    final impl = BasicCrocOTTPlayer.auth(host: _host, login: login, password: password);
    return impl.requestDevice(name);
  }

  // get devices with code
  Future<List<DeviceInfo>> getDevicesByCode(String code) {
    final impl = BasicCrocOTTPlayer.code(host: _host, code: code);
    return impl.getDevices();
  }

  // request with code
  Future<DeviceInfo> requestDeviceByCode(String code, String name) {
    final impl = BasicCrocOTTPlayer.code(host: _host, code: code);
    return impl.requestDevice(name);
  }

  // token
  Future<bool> incPlayingStream(String? pid, String stream) {
    if (pid == null) {
      return Future.value(true);
    }

    final resp = fetchPost('/client/content/$pid/live/$stream/views', {});
    return resp.then((response) {
      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      return false;
    }, onError: (error) {
      return false;
    });
  }

  Future<int> getStremViewCount(String? pid, String stream) {
    if (pid == null) {
      return Future.value(0);
    }

    final resp = fetchGet('/client/content/$pid/live/$stream/views');
    return resp.then((value) {
      final respData = httpDataResponseFromString(value.body);
      if (respData == null) {
        throw ErrorHttp(value.statusCode, value.reasonPhrase);
      }

      if (value.statusCode != HttpStatus.ok) {
        return 0;
      }

      final data = respData.contentDynamic()!;
      return data;
    }, onError: (error) {
      return 0;
    });
  }

  Future<bool> incPlayingVod(String? pid, String stream) {
    if (pid == null) {
      return Future.value(true);
    }

    final resp = fetchPost('/client/content/$pid/vod/$stream/views', {});
    return resp.then((response) {
      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      return false;
    }, onError: (error) {
      return false;
    });
  }

  Future<int> getVodViewCount(String? pid, String stream) {
    if (pid == null) {
      return Future.value(0);
    }

    final resp = fetchGet('/client/content/$pid/vod/$stream/views');
    return resp.then((value) {
      final respData = httpDataResponseFromString(value.body);
      if (respData == null) {
        throw ErrorHttp(value.statusCode, value.reasonPhrase);
      }

      if (value.statusCode != HttpStatus.ok) {
        return 0;
      }

      final data = respData.contentDynamic()!;
      return data;
    }, onError: (error) {
      return 0;
    });
  }

  Future<bool> incPlayingEpisode(String? pid, String stream) {
    if (pid == null) {
      return Future.value(true);
    }

    final resp = fetchPost('/client/content/$pid/episode/$stream/views', {});
    return resp.then((response) {
      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      return false;
    }, onError: (error) {
      return false;
    });
  }

  Future<int> getEpisodeViewCount(String? pid, String stream) {
    if (pid == null) {
      return Future.value(0);
    }

    final resp = fetchGet('/client/content/$pid/episode/$stream/views');
    return resp.then((value) {
      final respData = httpDataResponseFromString(value.body);
      if (respData == null) {
        throw ErrorHttp(value.statusCode, value.reasonPhrase);
      }

      if (value.statusCode != HttpStatus.ok) {
        return 0;
      }

      final data = respData.contentDynamic()!;
      return data;
    }, onError: (error) {
      return 0;
    });
  }

  Future<bool> interruptStream(String? pid, String sid, int msec) {
    if (pid == null) {
      return Future.value(true);
    }

    final link = '/client/content/$pid/live/$sid/interrupt';
    final resp = fetchPost(link, {"interrupt": msec});
    return resp.then((response) {
      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      return false;
    }, onError: (error) {
      return false;
    });
  }

  Future<bool> interruptVod(String? pid, String sid, int msec) {
    if (pid == null) {
      return Future.value(true);
    }

    final link = '/client/content/$pid/vod/$sid/interrupt';
    final resp = fetchPost(link, {"interrupt": msec});
    return resp.then((response) {
      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      return false;
    }, onError: (error) {
      return false;
    });
  }

  Future<bool> interruptEpisode(String? pid, String sid, int msec) {
    if (pid == null) {
      return Future.value(true);
    }

    final link = '/client/content/$pid/episode/$sid/interrupt';
    final resp = fetchPost(link, {"interrupt": msec});
    return resp.then((response) {
      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      return false;
    }, onError: (error) {
      return false;
    });
  }

  // public
  Uri getSubscribeUri(String pid, String sid) {
    final url = getBackendEndpoint('/package/$pid/checkout/$sid');
    return url;
  }

  ClientInfo getDeviceInfo(String did);

  // basic
  Future<Tokens?> login(String login, String password, String device) {
    final client = getDeviceInfo(device);
    final impl = BasicCrocOTTPlayer.auth(host: _host, login: login, password: password);
    final res = impl.login(client);
    res.then((token) {
      if (token == null) {
        return;
      }
      _setTokens(token);
    });
    return res;
  }

  // login with code
  Future<Tokens?> loginByCode(String code, String device) {
    final client = getDeviceInfo(device);
    final impl = BasicCrocOTTPlayer.code(host: _host, code: code);
    final res = impl.login(client);
    res.then((token) {
      if (token == null) {
        return;
      }
      _setTokens(token);
    });
    return res;
  }

  // token
  Future<SubProfile> profileWithToken() {
    final resp = fetchGet('/client/profile');

    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return SubProfile.fromJson(data);
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return;
    });
  }

  Future<bool> changePlaylistSelectStatus(String id, bool select) {
    final link = '/client/playlist/select/$id';
    final resp = fetchPost(link, {'select': select});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return data['select'];
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  Future<bool> addFavoriteStream(String pid, String sid, bool value) {
    final link = '/client/content/$pid/live/$sid/favorite';
    final resp = fetchPost(link, {"favorite": value});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  Future<bool> addRecentStream(String pid, String sid, int msec) {
    final link = '/client/content/$pid/live/$sid/recent';
    final resp = fetchPost(link, {"recent": msec});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  Future<bool> addFavoriteVod(String pid, String sid, bool value) {
    final link = '/client/content/$pid/vod/$sid/favorite';
    final resp = fetchPost(link, {"favorite": value});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  Future<bool> addRecentVod(String pid, String sid, int msec) {
    final link = '/client/content/$pid/vod/$sid/recent';
    final resp = fetchPost(link, {"recent": msec});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  Future<bool> addFavoriteEpisode(String pid, String sid, bool value) {
    final link = '/client/content/$pid/episode/$sid/favorite';
    final resp = fetchPost(link, {"favorite": value});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  Future<bool> addRecentEpisode(String pid, String sid, int msec) {
    final link = '/client/content/$pid/episode/$sid/recent';
    final resp = fetchPost(link, {"recent": msec});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  Future<bool> addFavoriteSerial(String pid, String sid, bool value) {
    final link = '/client/content/$pid/serial/$sid/favorite';
    final resp = fetchPost(link, {"favorite": value});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  Future<bool> addRecentSerial(String pid, String sid, int msec) {
    final link = '/client/content/$pid/serial/$sid/recent';
    final resp = fetchPost(link, {"recent": msec});
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        return true;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
      return false;
    });
  }

  // token
  Future<List<ProgrammeInfo>> getEpg(String eid) {
    // eid => epg id
    final resp = fetchGet('/client/content/$eid/epg');
    return resp.then((response) async {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<ProgrammeInfo> result = [];
        final progr = data['epg'];
        for (final item in progr) {
          final pack = ProgrammeInfo.fromJson(item);
          result.add(pack);
        }
        return result;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<ChannelInfo?> getStream(String? pid, String sid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/live/$sid');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return ChannelInfo.fromJson(data);
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<CatchupInfo?> getCatchup(String? pid, String cid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/catchup/$cid');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return CatchupInfo.fromJson(data);
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<List<CatchupInfo>?> getCatchups(String? pid, String lid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/live/$lid/catchups');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentList()!;
        final List<CatchupInfo> result = [];
        for (final item in data) {
          final pack = CatchupInfo.fromJson(item);
          result.add(pack);
        }
        return result;
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<VodInfo?> getVod(String? pid, String vid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/vod/$vid');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return VodInfo.fromJson(data);
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<SeasonInfo?> getSeason(String? pid, String sid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/season/$sid');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return SeasonInfo.fromJson(data);
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<List<VodInfo>?> getSeasonEpisodes(String? pid, String sid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/season/$sid/episodes');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentList()!;
        final List<VodInfo> result = [];
        for (final item in data) {
          final pack = VodInfo.fromJson(item);
          result.add(pack);
        }
        return result;
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<VodInfo?> getEpisode(String? pid, String eid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/episode/$eid');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return VodInfo.fromJson(data);
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<List<SeasonInfo>?> getSerialSeasons(String? pid, String sid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/serial/$sid/seasons');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentList()!;
        final List<SeasonInfo> result = [];
        for (final item in data) {
          final pack = SeasonInfo.fromJson(item);
          result.add(pack);
        }
        return result;
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<SerialInfo?> getSerial(String? pid, String sid) {
    if (pid == null) {
      return Future.value();
    }

    final resp = fetchGet('/client/content/$pid/serial/$sid');
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return SerialInfo.fromJson(data);
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<(List<OttPackageInfo>, List<PackagePublic>)> ottPackagesWithToken() {
    final resp = fetchGet('/client/ott');
    return resp.then((response) async {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<OttPackageInfo> packages = [];
        for (final package in data['packages']) {
          final pack = OttPackageInfo.fromJson(package);
          packages.add(pack);
        }
        final List<PackagePublic> available = [];
        for (final package in data['available']) {
          final pack = PackagePublic.fromJson(package);
          available.add(pack);
        }
        return (packages, available);
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<(List<OttPackageInfo>, List<PackagePublic>)> iptvPackagesWithToken() {
    final resp = fetchGet('/client/iptv');
    return resp.then((response) async {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<OttPackageInfo> packages = [];
        for (final package in data['packages']) {
          final pack = OttPackageInfo.fromJson(package);
          packages.add(pack);
        }
        final List<PackagePublic> available = [];
        for (final package in data['available']) {
          final pack = PackagePublic.fromJson(package);
          available.add(pack);
        }
        return (packages, available);
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<(List<OttPackageInfo>, List<PackagePublic>)> packagesWithToken() {
    final resp = fetchGet('/client/content');
    return resp.then((response) async {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<OttPackageInfo> packages = [];
        for (final package in data['packages']) {
          final pack = OttPackageInfo.fromJson(package);
          packages.add(pack);
        }
        final List<PackagePublic> available = [];
        for (final package in data['available']) {
          final pack = PackagePublic.fromJson(package);
          available.add(pack);
        }
        return (packages, available);
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<List<ChannelInfo>> lives() {
    final resp = fetchGet('/client/content/lives');
    return resp.then((response) async {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<ChannelInfo> result = [];
        final streams = data['streams'];
        for (final stream in streams) {
          final live = ChannelInfo.fromJson(stream);
          result.add(live);
        }
        return result;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<List<VodInfo>> vods() {
    final resp = fetchGet('/client/content/vods');
    return resp.then((response) async {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<VodInfo> result = [];
        final vods = data['vods'];
        for (final vod in vods) {
          final live = VodInfo.fromJson(vod);
          result.add(live);
        }
        return result;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<List<SerialInfo>> serials() {
    final resp = fetchGet('/client/content/serials');
    return resp.then((response) async {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<SerialInfo> result = [];
        final serials = data['serials'];
        for (final serial in serials) {
          final ser = SerialInfo.fromJson(serial);
          result.add(ser);
        }
        return result;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<List<PackagePublic>> publicPackages() {
    const url = '/client/content/packages';
    final resp = fetchGet(url);
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentList()!;
        final List<PackagePublic> result = [];
        for (final pack in data) {
          final pub = PackagePublic.fromJson(pack);
          result.add(pub);
        }
        return result;
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<OttPackageInfo?> getPackage({required String pid}) {
    final url = '/client/content/$pid';
    final resp = fetchGet(url);
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return OttPackageInfo.fromJson(data);
      }

      return null;
    }, onError: (error) {
      return null;
    });
  }

  Future<LivePageResp> paginatePidLives(
      {required String pid, required int limit, required int offset, String? group}) {
    var url = '/client/content/$pid/lives?limit=$limit&offset=$offset';
    if (group != null && group.isNotEmpty) {
      url += "&group=$group";
    }
    final resp = fetchGet(url);
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<ChannelInfo> streams = [];
        for (final item in data['streams']) {
          streams.add(ChannelInfo.fromJson(item));
        }
        return LivePageResp(streams, CountAndVersionLO.fromJson(data));
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<VodsPageResp> paginatePidVods(
      {required String pid, required int limit, required int offset, String? group}) {
    var url = '/client/content/$pid/vods?limit=$limit&offset=$offset';
    if (group != null && group.isNotEmpty) {
      url += "&group=$group";
    }
    final resp = fetchGet(url);
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<VodInfo> vods = [];
        for (final item in data['vods']) {
          vods.add(VodInfo.fromJson(item));
        }
        return VodsPageResp(vods, CountAndVersionLO.fromJson(data));
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  Future<SerialsPageResp> paginatePidSerials(
      {required String pid, required int limit, required int offset, String? group}) {
    var url = '/client/content/$pid/serials?limit=$limit&offset=$offset';
    if (group != null && group.isNotEmpty) {
      url += "&group=$group";
    }
    final resp = fetchGet(url);
    return resp.then((response) {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        throw ErrorHttp(response.statusCode, response.reasonPhrase);
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        final List<SerialInfo> vods = [];
        for (final item in data['serials']) {
          vods.add(SerialInfo.fromJson(item));
        }
        return SerialsPageResp(vods, CountAndVersionLO.fromJson(data));
      }

      throw ErrorHttp(response.statusCode, response.reasonPhrase, respData.error());
    }, onError: (error) {
      handleError(error);
    });
  }

  bool isNeedHelpFromResp(http.Response response) {
    if (response.statusCode == HttpStatus.unauthorized) {
      final respData = httpDataResponseFromString(response.body);
      final error = respData!.error()!;
      if (error.code == ErrorCodes.kErrAuth) {
        return true;
      }
    } else if (response.statusCode == HttpStatus.forbidden) {
      final respData = httpDataResponseFromString(response.body);
      final error = respData!.error()!;
      if (error.code == ErrorCodes.kErrForrbiddenByConflict ||
          error.code == ErrorCodes.kClientErrBannedDevice) {
        return true;
      }
    }

    return false;
  }

  Future<http.Response> fetchGet(String path) async {
    final http.Response response = await http.get(getBackendEndpoint(path), headers: _getHeaders());
    if (isNeedHelpFromResp(response)) {
      if (_tokens == null) {
        throw NeedHelpError('login');
      }

      final body = json.encode({'refresh_token': _tokens!.refresh});
      final resp = await http.post(getBackendEndpoint('/client/refresh_token'), body: body);
      if (resp.statusCode == HttpStatus.ok) {
        // all codes on_refresh_token without 200 it is error
        final respData = httpDataResponseFromString(resp.body);
        final data = respData!.contentMap()!;
        _setTokens(Tokens(refresh: data['refresh_token'], access: data['access_token']));
      } else {
        throw NeedHelpError('login');
      }
      return http.get(getBackendEndpoint(path), headers: _getHeaders());
    }
    return response;
  }

  Future<http.Response> fetchPost(String path, Map<String, dynamic> data) async {
    final bodyJson = json.encode(data);
    final response =
        await http.post(getBackendEndpoint(path), headers: _getJsonHeaders(), body: bodyJson);
    if (isNeedHelpFromResp(response)) {
      if (_tokens == null) {
        throw NeedHelpError('login');
      }

      final body = json.encode({'refresh_token': _tokens!.refresh});
      final resp = await http.post(getBackendEndpoint('/client/refresh_token'), body: body);
      if (resp.statusCode == HttpStatus.ok) {
        // all codes on_refresh_token without 200 it is error
        final respData = httpDataResponseFromString(resp.body);
        final data = respData!.contentMap()!;
        _setTokens(Tokens(refresh: data['refresh_token'], access: data['access_token']));
      } else {
        throw NeedHelpError('login');
      }
      return http.post(getBackendEndpoint(path), headers: _getJsonHeaders(), body: bodyJson);
    }
    return response;
  }

  // Future<http.Response> fetchPatch(String path, Map<String, dynamic> data) {
  //   final Map<String, String> headers = _getJsonHeaders();
  //   final body = json.encode(data);
  //   final response = http.patch(getBackendEndpoint(path), headers: headers, body: body);
  //   return response;
  // }

  // private:
  Map<String, String> _getJsonHeaders() {
    final Map<String, String> headers = {
      'content-type': 'application/json',
      'accept': 'application/json'
    };
    if (_tokens != null) {
      headers[HttpHeaders.authorizationHeader] = 'Bearer ${_tokens!.access}';
    }
    return headers;
  }

  Map<String, String> _getHeaders() {
    final Map<String, String> headers = {};
    if (_tokens != null) {
      headers[HttpHeaders.authorizationHeader] = 'Bearer ${_tokens!.access}';
    }
    return headers;
  }

  Future<IAdService?> getAdService() {
    final resp = fetchGet('/client/content/ads');
    return resp.then((response) async {
      if (isNeedHelpFromResp(response)) {
        final help = NeedHelpError('login');
        needHelp(help);
        throw help;
      }

      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        return null;
      }

      if (response.statusCode == HttpStatus.ok) {
        final data = respData.contentMap()!;
        return IAdService.fromJson(data);
      }

      return null;
    }, onError: (error) {
      handleError(error);
    });
  }

  // protected:
  void needHelp(NeedHelpError help) {
    if (onNeedHelp != null) {
      onNeedHelp!.call();
    }
  }

  void handleError(dynamic error) {
    if (error is NeedHelpError) {
      needHelp(error);
      throw error;
    }
    handleHttpError(error);
  }

  Uri getBackendEndpoint(String path) {
    final String base = '$directHost$_API_VERSION';
    return Uri.parse('$base$path');
  }
}
