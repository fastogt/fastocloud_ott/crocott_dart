class CountAndVersion {
  static const _COUNT_FIELD = 'count';
  static const _VERSION_FIELD = 'version';

  final int count;
  final int version;

  CountAndVersion({required this.count, required this.version});

  CountAndVersion copy() {
    return CountAndVersion(count: count, version: version);
  }

  factory CountAndVersion.fromJson(Map<String, dynamic> json) {
    final count = json[_COUNT_FIELD];
    final version = json[_VERSION_FIELD];
    return CountAndVersion(count: count, version: version);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {_COUNT_FIELD: count, _VERSION_FIELD: version};
    return result;
  }
}

class CountAndVersionLO extends CountAndVersion {
  static const _LIMIT_FIELD = 'limit';
  static const _OFFSET_FIELD = 'offset';

  final int limit;
  final int offset;

  CountAndVersionLO(
      {required int count, required int version, required this.limit, required this.offset})
      : super(count: count, version: version);

  CountAndVersionLO copy() {
    return CountAndVersionLO(count: count, version: version, limit: limit, offset: offset);
  }

  factory CountAndVersionLO.fromJson(Map<String, dynamic> json) {
    final base = CountAndVersion.fromJson(json);
    final limit = json[_LIMIT_FIELD];
    final offset = json[_OFFSET_FIELD];
    return CountAndVersionLO(
        count: base.count, version: base.version, limit: limit, offset: offset);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[_LIMIT_FIELD] = limit;
    result[_OFFSET_FIELD] = offset;
    return result;
  }
}

class PackageDBStats {
  static const _STREAMS_FIELD = 'streams';
  static const _CATCHUPS_FIELD = 'catchups';
  static const _VODS_FIELD = 'vods';
  static const _EPISODES_FIELD = 'episodes';
  static const _SEASONS_FIELD = 'seasons';
  static const _SERIALS_FIELD = 'serials';

  final CountAndVersion streams;
  final CountAndVersion catchups;
  final CountAndVersion vods;
  final CountAndVersion episodes;
  final CountAndVersion seasons;
  final CountAndVersion serials;

  PackageDBStats(
      {required this.streams,
      required this.catchups,
      required this.vods,
      required this.episodes,
      required this.seasons,
      required this.serials});

  PackageDBStats copy() {
    return PackageDBStats(
        streams: streams,
        catchups: catchups,
        vods: vods,
        episodes: episodes,
        seasons: seasons,
        serials: serials);
  }

  factory PackageDBStats.fromJson(Map<String, dynamic> json) {
    final streams = json[_STREAMS_FIELD];
    final catchups = json[_CATCHUPS_FIELD];
    final vods = json[_VODS_FIELD];
    final episodes = json[_EPISODES_FIELD];
    final seasons = json[_SEASONS_FIELD];
    final serials = json[_SERIALS_FIELD];
    return PackageDBStats(
        streams: CountAndVersion.fromJson(streams),
        catchups: CountAndVersion.fromJson(catchups),
        vods: CountAndVersion.fromJson(vods),
        episodes: CountAndVersion.fromJson(episodes),
        seasons: CountAndVersion.fromJson(seasons),
        serials: CountAndVersion.fromJson(serials));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      _STREAMS_FIELD: streams.toJson(),
      _CATCHUPS_FIELD: catchups.toJson(),
      _VODS_FIELD: vods.toJson(),
      _EPISODES_FIELD: episodes.toJson(),
      _SEASONS_FIELD: seasons.toJson(),
      _SERIALS_FIELD: serials.toJson()
    };
    return result;
  }
}
